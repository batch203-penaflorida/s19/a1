let username;
let password;
let role;

function loginUser() {
  let setUsername = prompt("Enter your username: ");
  let setPassword = prompt("Enter your password: ");
  let setRole = prompt("Enter your role: ");
  if (
    setUsername === "" ||
    setUsername === null ||
    setUsername === undefined ||
    setPassword === "" ||
    setPassword === null ||
    setPassword === undefined ||
    setRole === "" ||
    setRole === null ||
    setRole === undefined
  ) {
    alert("Input must not be empty");
    loginUser();
  } else {
    switch (setRole) {
      case "admin":
        alert("Welcome back to the class portal, admin!");
        break;
      case "teacher":
        alert("Thank you for logging in, teacher!");
        break;
      case "student":
        alert("Welcome to the class portal, student!");
        break;
      default:
        alert("Role out of range.");
        loginUser();
    }
  }
}
loginUser();

function checkAverage(num1, num2, num3, num4) {
  let sum = num1 + num2 + num3 + num4;
  let setAverage = sum / 4;
  if (setAverage <= 74) {
    console.log("Hello, student, your average is . The letter equivalent is F");
  } else if (setAverage >= 75 && setAverage <= 79) {
    console.log("Hello, student, your average is . The letter equivalent is D");
  } else if (setAverage >= 80 && setAverage <= 84) {
    console.log("Hello, student, your average is . The letter equivalent is C");
  } else if (setAverage >= 85 && setAverage <= 89) {
    console.log("Hello, student, your average is . The letter equivalent is B");
  } else if (setAverage >= 90 && setAverage <= 95) {
    console.log("Hello, student, your average is . The letter equivalent is A");
  } else if (setAverage >= 96 && setAverage <= 100) {
    ("Hello, student, your average is . The letter equivalent is A+");
  }
}
console.log("checkAverage(71,70,73,74)");
checkAverage(71, 70, 73, 74);
console.log("checkAverage(75,75,76,78)");
checkAverage(75, 75, 76, 78);
console.log("checkAverage(80,81,82,78)");
checkAverage(80, 81, 82, 78);
console.log("checkAverage(84,85,87,88)");
checkAverage(84, 85, 87, 88);
console.log("checkAverage(89,90,91,90)");
checkAverage(89, 90, 91, 90);
console.log("checkAverage(91,96,97,95)");
checkAverage(91, 96, 97, 95);
console.log("checkAverage(91,96,97,99)");
